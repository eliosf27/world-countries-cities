import pandas as pd
import json
import geohash as gh

brasil = 33
mexico = 159
colombia = 53 
argentina = 10
peru = 194
chile = 48
ecuador = 70
guatemala = 100
haiti = 105
bolivia = 29
republica_dominicana = 68
honduras = 108
paraguay = 193
nicaragua = 179
el_salvador = 72
costa_rica = 59
panama = 190
puerto_rico = 199
uruguay = 256

valid_countries_ids = [
	brasil, mexico, colombia, 
	argentina, peru, chile, 
	ecuador, guatemala, haiti, 
	bolivia, republica_dominicana, honduras, 
	paraguay, nicaragua, el_salvador, 
	costa_rica, panama, puerto_rico, 
	uruguay
]

actives_countries_ids = [
	brasil, mexico, colombia, argentina
]

def countries():
	df = pd.read_csv('countries.csv', sep=',', names=["CountryId","Country","FIPS104","ISO2","ISO3","ISON","Internet","Capital","MapReference","NationalitySingular","NationalityPlural","Currency","CurrencyCode","Population","Title","Comment"])
	list_data = []  
	for country_id, country, iso in df[['CountryId', 'Country', 'ISO3']].values:
		if country != 'Country' and iso != 'ISO3':
			if int(country_id) in valid_countries_ids:
				country_model = {
				    "model": "main.countries",
				    "pk": country_id,
				    "fields": {
				        "name": country,
				        "code": iso,
				        "is_enabled": int(country_id) in actives_countries_ids,
				        "created_at": "2018-4-13T16:06:07.337Z",
				        "updated_at": "2018-4-13T17:00:00.258Z"
				    }
				}
				list_data.append(country_model)

	return json.dumps(list_data)


def cities():
	df = pd.read_csv('cities.csv', sep=',', names=["CityId","CountryID","RegionID","City","Latitude","Longitude","TimeZone","DmaId","Code"])

	list_data = []  
	for city_id, country_id, city, lat, lng in df[["CityId","CountryID", "City","Latitude","Longitude"]].values:
		if city_id != 'CityId' and country_id != 'CountryID':
			if int(country_id) in valid_countries_ids:
				country_model = {
				    "model": "main.cities",
				    "pk": city_id,
				    "fields": {
				    	"country": country_id,
				        "name": city,
				        "lat": float(lat),
				        "lng": float(lng),
				        "geohash": gh.encode(float(lat), float(lng), precision=3),
				        "is_enabled": int(country_id) in actives_countries_ids,
				        "created_at": "2018-4-13T16:06:07.337Z",
				        "updated_at": "2018-4-13T17:00:00.258Z"
				    }
				}
				list_data.append(country_model)

	return json.dumps(list_data)


# print(countries())
print(cities())